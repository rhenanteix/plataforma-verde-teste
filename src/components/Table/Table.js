import React from "react";
import "./Table.css";
import EditImg from "../../assets/img/edit.png";
import DeleteImg from "../../assets/img/delete.png";

const Table = (props) => {
  function excluir(id) {
    const index = props.line.findIndex((elm) => {
      return elm.id === id;
    });
    props.line.splice(props.line.indexOf(index), 1);
    props.excluirLinha(props.line);
  }
  function formatarDataParaEditar(data) {
    const [day, month, year] = data.match(/\d+/g);
    return `${year}-${month}-${day}`;
  }
  function editar(id) {
    props.editarLinha(true);
    const form = document.forms.registration;
    const [nome, cpf, date, state, city] = form;
    const index = props.line.findIndex((elm) => {
      return elm.id === id;
    });
    nome.value = props.line[index].nome;
    cpf.value = props.line[index].cpf;
    date.value = formatarDataParaEditar(props.line[index].data);
    state.value = props.line[index].estado;
    const optionSelected = state.options[state.selectedIndex];
    const idEstado = parseInt(optionSelected.dataset.id);
    fetch(
      `https://servicodados.ibge.gov.br/api/v1/localidades/estados/${idEstado}/municipios`
    ).then((resp) => {
      if (resp.status === 200) {
        resp.json().then((json) => {
          props.setCidade(json);
          city.value = props.line[index].cidade;
        });
      }
    });
    props.searchIndex(index);
  }

  return (
    <div className="conteudo-table">
      <table className="tabela">
        <thead>
          <tr>
            <th>Nome Completo</th>
            <th>CPF</th>
            <th>Data</th>
            <th>Idade</th>
            <th>Estado</th>
            <th>Cidade</th>
            <th>Editar</th>
            <th>Excluir</th>
          </tr>
        </thead>
        <tbody>
          {props.line.map((itens) => {
            return (
              <tr key={itens.id}>
                <td>{itens.nome}</td>
                <td>{itens.cpf}</td>
                <td>{itens.data}</td>
                <td>{itens.idade}</td>
                <td>{itens.estado}</td>
                <td>{itens.cidade}</td>
                <td onClick={(_) => editar(itens.id)}>
                  <img
                    className="editar"
                    alt="editar usuario"
                    src={EditImg}
                  />
                </td>
                <td onClick={(_) => excluir(itens.id)}>
                  <img
                    className="excluir"
                    alt="excluir usuario"
                    src={DeleteImg}
                  />
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};

export default Table;
