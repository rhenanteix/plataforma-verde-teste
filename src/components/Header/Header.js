import React from "react";
import ImageLogo from "../../assets/img/icon.png";
import Logo from "../../assets/img/logo.png";
import "./Header.css";

const Header = () => {
  return (
      <>
    <div className="menu">
      <img className="logo-principal" src={Logo}></img>  
    </div>  
    <div className="container-header-sub">
      <img src={ImageLogo}></img>
      <h1>Cadastro Usuário</h1>
    </div>
    </>
  );
};

export default Header;
