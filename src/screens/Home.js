import React from "react";
import Formulario from "../components/Form/Form";
import Header from "../components/Header/Header"
import "./Home.css";

const Home = () => {
  return (
    <>
    <Header/>
    <section className="content">
      <div className="header">
      </div>
      <Formulario />
    </section>
    </>
  );
};

export default Home;
